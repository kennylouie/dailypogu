module gitlab.com/kennylouie/dailypogu

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
	go.uber.org/zap v1.14.1
)
