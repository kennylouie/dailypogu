package main

import (
	"fmt"
	"log"

	"gitlab.com/kennylouie/dailypogu/pkg/api"
	"gitlab.com/kennylouie/dailypogu/pkg/conf"
	"gitlab.com/kennylouie/dailypogu/pkg/datastore"

	"go.uber.org/zap"
)

func main() {
	// start global logger
	c := zap.NewProductionConfig()
	c.OutputPaths = []string{"stdout"}
	logger, err := c.Build()
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not init zap logger: %v", err))
	}
	defer logger.Sync()

	config := conf.NewConf(logger.Named("conf_logger"))

	ds := datastore.NewCache()

	api := api.NewApi(logger.Named("api_logger"), config, ds)
	api.Init()
	api.Serve()
}
