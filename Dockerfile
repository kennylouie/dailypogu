FROM golang:1.14.1-alpine3.11 as BUILDER

WORKDIR /dailypogu

ADD . .

RUN go build -o dailypogu ./cmd/dailypogu/dailypogu.go

FROM alpine:3.11

COPY --from=BUILDER /dailypogu/dailypogu /bin/.

CMD ["dailypogu"]
