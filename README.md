# dailypogu

## Overview

A blog service to host reviews by authors with commenting feature per review.

## Usage

Latest build releases can be pulled from dockerhub.
Major, Major.Minor, and Major.Minor.Patch are available.

```
docker pull kennylouie/dailypogu:latest
docker run --rm -d -e API_PORT=3000 -p 3000:3000 kennylouie/dailypogu:latest
```

## Endpoints

Reads all reviews
`GET /api/v1/reviews`

Returns a map with keys as uuids and values as reviews
Response:
```
{
    "__UUID__": {"Author", "author", "Title", "title", "Article": "Some text"}
}
```

Create a new review
`POST /api/v1/reviews`
```
{
    "Author": "author",
    "Title": "title",
    "Article": "Some text"
}
```
An id is returned on successful creation
Response:
```
"__UUID__"
```

Read a single review
`GET /api/v1/reviews/:id`

Update a single review
`PUT /api/v1/reviews/:id` 
```
{
    "Author": "author",
    "Title": "title",
    "Article": "Some text"
}
```

Delete a review
`DELETE /api/v1/reviews/:id`

Read all comments of a review
`GET /api/v1/reviews/:id/comments`

Returns a map with keys as uuids and values as comments
Response:
```
{
    "__UUID__": {"Author", "author", "Comment": "a comment"}
}
```

Create a new comment of a review
`POST /api/v1/reviews/:id/comments`
```
{
    "Author": "author",
    "Comment": "a comment"
}
```
A comemnt id is returned on successful creation
Response:
```
"__UUID__"
```

Read a single comment of a review
`GET /api/v1/reviews/:id/comments/:cid`

Update a single comment
`PUT /api/v1/reviews/:id/comments/:cid`
```
{
    "Author": "author",
    "Comment": "a comment"
}
```

Delete a comment of a review
`DELETE /api/v1/reviews/:id/comments/:cid`

## Development

### Architecture

    +-----------------------------------------+
    |                       +--------------+  |
    |    +------------+     |    in-mem    |  |
    |    |    api     |<--->|  datastore   |  |
    |    +------------+     +--------------+  |
    |      (golang)            (golang)       |
    |                                         |
    |                            container    |
    +-----------------------------------------+

### Building from source

#### Dependencies

+ Go 1.14+
+ Docker

#### Running tests

```
go test -v ./...
```

#### Building and running image

Optional environment variables
```
export API_PORT=3000 // default is 3000
```

```
docker build -t dailypogu .
docker run --rm -d -e API_PORT=3000 -p 3000:3000 dailypogu
```
