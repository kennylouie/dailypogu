package api

import (
	"fmt"
	"io/ioutil"
	"log"

	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/kennylouie/dailypogu/pkg/conf"
	"gitlab.com/kennylouie/dailypogu/pkg/datastore"

	"go.uber.org/zap"
)

func TestApi(t *testing.T) {
	c := zap.NewProductionConfig()
	c.OutputPaths = []string{"stdout"}
	logger, err := c.Build()
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not init zap logger: %v", err))
	}
	defer logger.Sync()

	ds := datastore.NewCache()

	a := NewApi(logger, &conf.Conf{}, ds)
	a.Init()

	// simple tests for status codes

	// health endpiont
	req, _ := http.NewRequest(http.MethodGet, "/api/v1/health", nil)
	rr := httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// reviews endpoints

	// get all reviews
	req, _ = http.NewRequest(http.MethodGet, "/api/v1/reviews", nil)
	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// create review
	req, _ = http.NewRequest(http.MethodPost, "/api/v1/reviews", strings.NewReader(`{"Author": "test", "Article": "test", "Title": "test"}`))
	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// get review
	resp := rr.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	id := strings.ReplaceAll(string(body), "\"", "")
	id = strings.ReplaceAll(id, "\n", "")
	endpoint := fmt.Sprintf("/api/v1/reviews/%s", id)

	req, err = http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		t.Errorf("error in requesting with id %s", err)
	}

	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// update review
	req, err = http.NewRequest(http.MethodPut, endpoint, strings.NewReader(`{"Author": "test2", "Article": "test2", "Title2": "test2"}`))
	if err != nil {
		t.Errorf("error in requesting with id %s", err)
	}

	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// comments
	comments_endpoint := fmt.Sprintf("/api/v1/reviews/%s/comments", id)
	// get all comments
	req, _ = http.NewRequest(http.MethodGet, comments_endpoint, nil)
	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// create comment
	req, _ = http.NewRequest(http.MethodPost, comments_endpoint, strings.NewReader(`{"Author": "test", "Comment": "test"}`))
	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// get comment
	resp = rr.Result()
	body, _ = ioutil.ReadAll(resp.Body)
	cid := strings.ReplaceAll(string(body), "\"", "")
	cid = strings.ReplaceAll(cid, "\n", "")
	specific_comment_endpoint := fmt.Sprintf("/api/v1/reviews/%s/comments/%s", id, cid)

	req, err = http.NewRequest(http.MethodGet, specific_comment_endpoint, nil)
	if err != nil {
		t.Errorf("error in requesting with id %s", err)
	}

	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// update comment
	req, err = http.NewRequest(http.MethodPut, specific_comment_endpoint, strings.NewReader(`{"Author": "test2", "Comment": "test2"}`))
	if err != nil {
		t.Errorf("error in requesting with id %s", err)
	}

	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// delete comment
	req, err = http.NewRequest(http.MethodDelete, specific_comment_endpoint, nil)
	if err != nil {
		t.Errorf("error in requesting with id %s", err)
	}

	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}

	// delete review
	req, err = http.NewRequest(http.MethodDelete, endpoint, nil)
	if err != nil {
		t.Errorf("error in requesting with id %s", err)
	}

	rr = httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}
}
