package api

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/kennylouie/dailypogu/pkg/api/handlers"
	"gitlab.com/kennylouie/dailypogu/pkg/conf"
	"gitlab.com/kennylouie/dailypogu/pkg/datastore"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type Api struct {
	logger *zap.Logger
	router *mux.Router
	conf   *conf.Conf
	ds     *datastore.Cache
}

func NewApi(logger *zap.Logger, config *conf.Conf, ds *datastore.Cache) *Api {
	return &Api{
		logger: logger,
		router: mux.NewRouter().StrictSlash(true),
		conf:   config,
		ds:     ds,
	}
}

func (a *Api) Init() {
	err := a.load_routes()
	if err != nil {
		a.logger.Fatal("Could not load routes", zap.Error(err))
	}

	a.logger.Info("Loaded routes")
}

func (a *Api) Serve() {
	server := http.Server{
		Addr:         fmt.Sprintf(":%s", a.conf.API_PORT),
		Handler:      a.router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 20 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	a.logger.Info(fmt.Sprintf("Server serving on port %s", a.conf.API_PORT))

	log.Fatal(server.ListenAndServe())
}

func (a *Api) load_routes() error {
	v1 := a.router.PathPrefix("/api/v1").Subrouter()

	// utility routes
	health_handler := handlers.Health_handler{
		Logger: a.logger.Named("health_handler"),
	}
	v1.HandleFunc("/health", health_handler.Ping).Methods(http.MethodGet)

	// review routes
	reviews_router := v1.PathPrefix("/reviews").Subrouter()

	reviews_handler := handlers.Reviews_handler{
		Logger: a.logger.Named("reviews_handler"),
		DS:     a.ds,
	}

	reviews_router.HandleFunc("", reviews_handler.GetAllReviews).Methods(http.MethodGet)
	reviews_router.HandleFunc("", reviews_handler.CreateReview).Methods(http.MethodPost)
	reviews_router.HandleFunc("/{id}", reviews_handler.GetReview).Methods(http.MethodGet)
	reviews_router.HandleFunc("/{id}", reviews_handler.UpdateReview).Methods(http.MethodPut)
	reviews_router.HandleFunc("/{id}", reviews_handler.DeleteReview).Methods(http.MethodDelete)

	// comments routes
	reviews_router.HandleFunc("/{id}/comments", reviews_handler.GetAllComments).Methods(http.MethodGet)
	reviews_router.HandleFunc("/{id}/comments", reviews_handler.CreateComment).Methods(http.MethodPost)
	reviews_router.HandleFunc("/{id}/comments/{cid}", reviews_handler.GetComment).Methods(http.MethodGet)
	reviews_router.HandleFunc("/{id}/comments/{cid}", reviews_handler.UpdateComment).Methods(http.MethodPut)
	reviews_router.HandleFunc("/{id}/comments/{cid}", reviews_handler.DeleteComment).Methods(http.MethodDelete)

	return nil
}
