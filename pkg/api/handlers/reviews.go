package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/kennylouie/dailypogu/pkg/datastore"
	"gitlab.com/kennylouie/dailypogu/pkg/models"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type Reviews_handler struct {
	Logger *zap.Logger
	DS     *datastore.Cache
}

func (R *Reviews_handler) GetAllReviews(w http.ResponseWriter, r *http.Request) {
	reviews, err := R.DS.GetAllReviews()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error("Could not get all reviews", zap.Error(err))
		return
	}

	json.NewEncoder(w).Encode(reviews)

	R.Logger.Info("retreived all reviews")
}

func (R *Reviews_handler) CreateReview(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error("Could not read request body", zap.Error(err))
		return
	}

	var review models.Review
	json.Unmarshal(body, &review)

	id, err := R.DS.CreateReview(&review)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error("Could not create review", zap.Error(err))
		return
	}

	json.NewEncoder(w).Encode(id)

	R.Logger.Info(fmt.Sprintf("created new review with id %s\n", id))
}

func (R *Reviews_handler) GetReview(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	review, err := R.DS.GetReview(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("could not get review for given id: %s\n", id), zap.Error(err))
		return
	}

	json.NewEncoder(w).Encode(review)

	R.Logger.Info(fmt.Sprintf("retrieved review with id: %s\n", id))
}

func (R *Reviews_handler) UpdateReview(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error("Could not read request body", zap.Error(err))
		return
	}

	var review models.Review
	json.Unmarshal(body, &review)

	err = R.DS.UpdateReview(id, &review)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("could not update review for id: %s\n", id), zap.Error(err))
		return
	}

	R.Logger.Info(fmt.Sprintf("Updated review %s\n", id))
}

func (R *Reviews_handler) DeleteReview(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	err := R.DS.DeleteReview(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("could not delete review for id: %s\n", id), zap.Error(err))
		return
	}

	R.Logger.Info(fmt.Sprintf("Deleted review %s\n", id))
}

func (R *Reviews_handler) GetAllComments(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	comments, err := R.DS.GetAllComments(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("could not get all comments for id: %s\n", id), zap.Error(err))
		return
	}

	json.NewEncoder(w).Encode(comments)

	R.Logger.Info(fmt.Sprintf("retreived all comments for id: %s\n", id))
}

func (R *Reviews_handler) CreateComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error("Could not read request body", zap.Error(err))
		return
	}

	var comment models.Comment
	json.Unmarshal(body, &comment)

	cid, err := R.DS.CreateComment(id, &comment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("Could not create comment for id: %s\n", id), zap.Error(err))
		return
	}

	json.NewEncoder(w).Encode(cid)

	R.Logger.Info(fmt.Sprintf("created new comment with id %s for review %s\n", cid, id))
}

func (R *Reviews_handler) GetComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	cid, Ok := vars["cid"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no comment id found in request query")
		return
	}

	comment, err := R.DS.GetComment(id, cid)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("could not get comment with id %s for review with id %s\n", cid, id), zap.Error(err))
		return
	}

	json.NewEncoder(w).Encode(comment)

	R.Logger.Info(fmt.Sprintf("retreived comment id %s from review id: %s\n", cid, id))
}

func (R *Reviews_handler) UpdateComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	cid, Ok := vars["cid"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no comment id found in request query")
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error("Could not read request body", zap.Error(err))
		return
	}

	var comment models.Comment
	json.Unmarshal(body, &comment)

	err = R.DS.UpdateComment(id, cid, &comment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("Could not update comment with id %s on review id %s\n", cid, id), zap.Error(err))
		return
	}

	R.Logger.Info(fmt.Sprintf("updated comment with id %s for review id %s\n", cid, id))
}

func (R *Reviews_handler) DeleteComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, Ok := vars["id"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no id found in request query")
		return
	}

	cid, Ok := vars["cid"]
	if !Ok {
		w.WriteHeader(http.StatusBadRequest)
		R.Logger.Error("no comment id found in request query")
		return
	}

	err := R.DS.DeleteComment(id, cid)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		R.Logger.Error(fmt.Sprintf("Could not delete comment with id %s on review id %s\n", cid, id), zap.Error(err))
		return
	}

	R.Logger.Info(fmt.Sprintf("deleted comment with id %s for review id %s\n", cid, id))
}
