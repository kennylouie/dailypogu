package models

type Review struct {
	Title    string              `json:"Title"`
	Article  string              `json:"Article"`
	Author   string              `json:"Author"`
	Comments map[string]*Comment `json:"Comments"`
}

type Comment struct {
	Comment string `json:"Comment"`
	Author  string `json:"Author"`
}
