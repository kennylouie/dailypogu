package conf

import (
	"github.com/kelseyhightower/envconfig"
	"go.uber.org/zap"
)

type Conf struct {
	// global conf for api
	API_PORT string `envconfig:"API_PORT" default:"3000"`
}

func NewConf(logger *zap.Logger) *Conf {
	c := &Conf{}

	err := envconfig.Process("", c)
	if err != nil {
		logger.Fatal("Could not get env vars", zap.Error(err))
	}

	logger.Info("Parsed env vars")

	return c
}
