package datastore

import (
	"testing"

	"gitlab.com/kennylouie/dailypogu/pkg/models"
)

func TestCreateCache(t *testing.T) {
	c := NewCache()

	if len(c.reviews) != 0 {
		t.Errorf("expected empty map but got %v\n", c.reviews)
	}
}

func TestGetAllReviews(t *testing.T) {
	c := NewCache()

	reviews, err := c.GetAllReviews()
	if err != nil {
		t.Errorf("could not get all reviews: %s\n", err)
	}

	if len(reviews) != 0 {
		t.Errorf("expected empty map but got %v\n", reviews)
	}
}

func TestNewReviewsUUID(t *testing.T) {
	c := NewCache()
	uuid := c.new_review_uuid()

	if uuid == "" {
		t.Errorf("expected a string but got nothing\n")
	}

	c.reviews[uuid] = &models.Review{}

	new_uuid := c.new_review_uuid()

	if new_uuid == uuid {
		t.Errorf("expected a different uuid but got the same: %s vs %s\n", new_uuid, uuid)
	}
}

func TestCreateReview(t *testing.T) {
	c := NewCache()

	expected := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(expected)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	if c.reviews[uuid].Author != expected.Author {
		t.Errorf("expected %s but got %s\n", expected.Author, c.reviews[uuid].Author)
	}

	if c.reviews[uuid].Article != expected.Article {
		t.Errorf("expected %s but got %s\n", expected.Article, c.reviews[uuid].Article)
	}

	if c.reviews[uuid].Title != expected.Title {
		t.Errorf("expected %s but got %s\n", expected.Title, c.reviews[uuid].Title)
	}

	if c.reviews[uuid].Comments == nil {
		t.Errorf("expected to have initialized comments map but got nil\n")
	}
}

func TestGetReview(t *testing.T) {
	c := NewCache()

	expected := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(expected)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	review, err := c.GetReview(uuid)
	if err != nil {
		t.Errorf("could not get review: %s\n", err)
	}

	if review.Author != expected.Author {
		t.Errorf("expected %s but got %s\n", expected.Author, c.reviews[uuid].Author)
	}

	if review.Article != expected.Article {
		t.Errorf("expected %s but got %s\n", expected.Article, c.reviews[uuid].Article)
	}

	if review.Title != expected.Title {
		t.Errorf("expected %s but got %s\n", expected.Title, c.reviews[uuid].Title)
	}

	if review.Comments == nil {
		t.Errorf("expected to have initialized comments map but got nil\n")
	}
}

func TestUUIDExistsReviews(t *testing.T) {
	c := NewCache()

	expected := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(expected)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	if !c.uuid_exists_reviews(uuid) {
		t.Errorf("expected uuid %s to exist in map %v\n", uuid, c.reviews)
	}
}

func TestCreateCommentsIfNotExists(t *testing.T) {
	c := NewCache()

	expected := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	c.reviews["test"] = expected

	if c.reviews["test"].Comments != nil {
		t.Errorf("should be a nil comment map initially\n")
	}

	c.create_comments_if_not_exists("test")

	if c.reviews["test"].Comments == nil {
		t.Errorf("expected an map: %v\n", c.reviews["test"])
	}

	if len(c.reviews["test"].Comments) != 0 {
		t.Errorf("expected an empty comments map: %v\n", c.reviews["test"].Comments)
	}
}
func TestUpdateReview(t *testing.T) {
	c := NewCache()

	initial := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(initial)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	expected := &models.Review{
		Author:  "test2",
		Article: "test2",
		Title:   "test2",
	}

	err = c.UpdateReview(uuid, expected)
	if err != nil {
		t.Errorf("could not update review: %s\n", err)
	}

	if c.reviews[uuid].Author != expected.Author {
		t.Errorf("expected %s but got %s\n", expected.Author, c.reviews[uuid].Author)
	}

	if c.reviews[uuid].Article != expected.Article {
		t.Errorf("expected %s but got %s", expected.Article, c.reviews[uuid].Article)
	}

	if c.reviews[uuid].Title != expected.Title {
		t.Errorf("expected %s but got %s\n", expected.Title, c.reviews[uuid].Title)
	}

	if c.reviews[uuid].Comments == nil {
		t.Errorf("expected to have initialized comments map but got nil")
	}
}

func TestDeleteReview(t *testing.T) {
	c := NewCache()

	initial := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(initial)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	err = c.DeleteReview(uuid)
	if err != nil {
		t.Errorf("could not delete review: %s\n", err)
	}

	if r, Ok := c.reviews[uuid]; Ok {
		t.Errorf("expected deleted review but got something: %v\n", r)
	}
}

func TestUUIDExistsComments(t *testing.T) {
	c := NewCache()

	expected := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(expected)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	c.reviews[uuid].Comments["test"] = &models.Comment{}

	if !c.uuid_exists_comments(uuid, "test") {
		t.Errorf("expected %s to be a key in %v\n", "test", c.reviews[uuid].Comments)
	}
}

func TestNewCommentsUUID(t *testing.T) {
	c := NewCache()

	uuid, err := c.CreateReview(&models.Review{})
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	c.reviews[uuid].Comments["test"] = &models.Comment{}

	new_cuuid := c.new_comment_uuid(uuid)

	if new_cuuid == "test" {
		t.Errorf("expected a different cuuid but got the same: %s vs %s\n", new_cuuid, "test")
	}
}

func TestCreateComment(t *testing.T) {
	c := NewCache()

	initial := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(initial)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	expected := &models.Comment{
		Author:  "test",
		Comment: "test",
	}

	cuuid, err := c.CreateComment(uuid, expected)
	if err != nil {
		t.Errorf("could not create comment: %s\n", err)
	}

	if author := c.reviews[uuid].Comments[cuuid].Author; author != expected.Author {
		t.Errorf("expected %s but got %s\n", author, expected.Author)
	}

	if comment := c.reviews[uuid].Comments[cuuid].Comment; comment != expected.Comment {
		t.Errorf("expected %s but got %s\n", comment, expected.Comment)
	}
}

func TestGetAllComments(t *testing.T) {
	c := NewCache()

	initial := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(initial)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	comments, err := c.GetAllComments(uuid)
	if err != nil {
		t.Errorf("could get all comments: %s\n", err)
	}

	if len(comments) != 0 {
		t.Errorf("expected empty map but got %v\n", comments)
	}

	expected := &models.Comment{
		Author:  "test",
		Comment: "test",
	}

	_, err = c.CreateComment(uuid, expected)
	if err != nil {
		t.Errorf("could not create comment: %s\n", err)
	}

	comments, err = c.GetAllComments(uuid)
	if err != nil {
		t.Errorf("could get all comments: %s\n", err)
	}

	if len(comments) != 1 {
		t.Errorf("expected 1 comment but got %v\n", comments)
	}
}

func TestGetComment(t *testing.T) {
	c := NewCache()

	initial := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
	}

	uuid, err := c.CreateReview(initial)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	expected := &models.Comment{
		Author:  "test",
		Comment: "test",
	}

	cuuid, err := c.CreateComment(uuid, expected)
	if err != nil {
		t.Errorf("could not create comment: %s\n", err)
	}

	comment, err := c.GetComment(uuid, cuuid)
	if err != nil {
		t.Errorf("could not read comment: %s\n", err)
	}

	if comment.Author != expected.Author {
		t.Errorf("expected %s but got %s\n", comment.Author, expected.Author)
	}

	if comment.Comment != expected.Comment {
		t.Errorf("expected %s but got %s\n", comment.Comment, expected.Comment)
	}
}

func TestUpdateComment(t *testing.T) {
	c := NewCache()

	initial := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
		Comments: map[string]*models.Comment{
			"test": &models.Comment{
				Author:  "test",
				Comment: "test",
			},
		},
	}

	uuid, err := c.CreateReview(initial)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	expected := &models.Comment{
		Author:  "test2",
		Comment: "test2",
	}

	err = c.UpdateComment(uuid, "test", expected)
	if err != nil {
		t.Errorf("could not update comment: %s\n", err)
	}

	comment, err := c.GetComment(uuid, "test")
	if err != nil {
		t.Errorf("could not read comment: %s\n", err)
	}

	if comment.Author != expected.Author {
		t.Errorf("expected %s but got %s\n", comment.Author, expected.Author)
	}

	if comment.Comment != expected.Comment {
		t.Errorf("expected %s but got %s\n", comment.Comment, expected.Comment)
	}
}

func TestDeleteComment(t *testing.T) {
	c := NewCache()

	initial := &models.Review{
		Author:  "test",
		Article: "test",
		Title:   "test",
		Comments: map[string]*models.Comment{
			"test": &models.Comment{
				Author:  "test",
				Comment: "test",
			},
		},
	}

	uuid, err := c.CreateReview(initial)
	if err != nil {
		t.Errorf("could not create review: %s\n", err)
	}

	err = c.DeleteComment(uuid, "test")
	if err != nil {
		t.Errorf("could not delete comment: %s\n", err)
	}

	comments, err := c.GetAllComments(uuid)
	if err != nil {
		t.Errorf("could not read all comments: %s\n", err)
	}

	if len(comments) != 0 {
		t.Errorf("expected an empty map but got %v\n", comments)
	}
}
