package datastore

import (
	"crypto/rand"
	"errors"
	"fmt"
	"log"
	"sync"

	"gitlab.com/kennylouie/dailypogu/pkg/models"
)

type Cache struct {
	mtx     sync.RWMutex
	reviews map[string]*models.Review
}

func NewCache() *Cache {
	var c Cache
	c.reviews = make(map[string]*models.Review)
	return &c
}

var (
	DOES_NOT_EXIST = errors.New("id does not exist in datastore")
)

func (c *Cache) GetAllReviews() (map[string]*models.Review, error) {
	c.mtx.RLock()
	defer c.mtx.RUnlock()

	return c.reviews, nil
}

func (c *Cache) GetReview(uuid string) (*models.Review, error) {
	c.mtx.RLock()
	defer c.mtx.RUnlock()

	if !c.uuid_exists_reviews(uuid) {
		return &models.Review{}, DOES_NOT_EXIST
	}

	return c.reviews[uuid], nil
}

func (c *Cache) CreateReview(r *models.Review) (string, error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	uuid := c.new_review_uuid()

	c.reviews[uuid] = r
	c.create_comments_if_not_exists(uuid)

	return uuid, nil
}

func (c *Cache) UpdateReview(uuid string, r *models.Review) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if !c.uuid_exists_reviews(uuid) {
		return DOES_NOT_EXIST
	}

	c.reviews[uuid] = r
	c.create_comments_if_not_exists(uuid)

	return nil
}

func (c *Cache) DeleteReview(uuid string) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if !c.uuid_exists_reviews(uuid) {
		return DOES_NOT_EXIST
	}

	delete(c.reviews, uuid)

	return nil
}

func (c *Cache) CreateComment(uuid string, comment *models.Comment) (string, error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if !c.uuid_exists_reviews(uuid) {
		return "", DOES_NOT_EXIST
	}

	cuuid := c.new_comment_uuid(uuid)

	c.reviews[uuid].Comments[cuuid] = comment

	return cuuid, nil
}

func (c *Cache) GetAllComments(uuid string) (map[string]*models.Comment, error) {
	c.mtx.RLock()
	defer c.mtx.RUnlock()

	if !c.uuid_exists_reviews(uuid) {
		return nil, DOES_NOT_EXIST
	}

	return c.reviews[uuid].Comments, nil
}

func (c *Cache) GetComment(uuid, cuuid string) (*models.Comment, error) {
	c.mtx.RLock()
	defer c.mtx.RUnlock()

	if !c.uuid_exists_reviews(uuid) {
		return nil, DOES_NOT_EXIST
	}

	if !c.uuid_exists_comments(uuid, cuuid) {
		return nil, DOES_NOT_EXIST
	}

	return c.reviews[uuid].Comments[cuuid], nil
}

func (c *Cache) UpdateComment(uuid, cuuid string, comment *models.Comment) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if !c.uuid_exists_reviews(uuid) {
		return DOES_NOT_EXIST
	}

	if !c.uuid_exists_comments(uuid, cuuid) {
		return DOES_NOT_EXIST
	}

	c.reviews[uuid].Comments[cuuid] = comment

	return nil
}

func (c *Cache) DeleteComment(uuid, cuuid string) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if !c.uuid_exists_reviews(uuid) {
		return DOES_NOT_EXIST
	}

	if !c.uuid_exists_comments(uuid, cuuid) {
		return DOES_NOT_EXIST
	}

	delete(c.reviews[uuid].Comments, cuuid)

	return nil
}

func (c *Cache) create_comments_if_not_exists(uuid string) {
	if c.reviews[uuid].Comments == nil {
		c.reviews[uuid].Comments = make(map[string]*models.Comment)
	}
}

func (c *Cache) uuid_exists_comments(uuid, cuuid string) bool {
	_, Ok := c.reviews[uuid].Comments[cuuid]
	return Ok
}

func (c *Cache) uuid_exists_reviews(uuid string) bool {
	_, Ok := c.reviews[uuid]
	return Ok
}

func (c *Cache) new_comment_uuid(uuid string) string {
	cuuid := new_uuid()

	for {
		if _, Ok := c.reviews[uuid].Comments[cuuid]; Ok {
			cuuid = new_uuid()
		}
		break
	}

	return cuuid
}

func (c *Cache) new_review_uuid() string {
	uuid := new_uuid()

	for {
		if _, Ok := c.reviews[uuid]; Ok {
			uuid = new_uuid()
		}
		break
	}

	return uuid
}

func new_uuid() string {
	b := make([]byte, 16)

	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}

	return fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
}
