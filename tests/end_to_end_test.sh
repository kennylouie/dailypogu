#!/bin/sh

HOST="http://docker"
# HOST="localhost"

main()
{
	need_cmd curl
	need_cmd jq

	# test empty reviews
	ensure assert_eq "$(get_all_reviews)" {}

	# test create single review
	local _id
	create_review

	local _review
	_review="$(get_review ${_id})"

	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Title")" test
	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Author")" test
	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Article")" test
	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Comments")" {}

	# test update review
	update_review "${_id}"
	_review="$(get_review ${_id})"

	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Title")" test2
	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Author")"
	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Article")"
	ensure assert_eq "$(echo "${_review}" | jq --raw-output ."Comments")" {}

	# test create comment
	local _cid
	create_comment "${_id}"

	_comment="$(get_comment "${_id}" "${_cid}")"

	ensure assert_eq "$(echo "${_comment}" | jq --raw-output ."Comment")" test
	ensure assert_eq "$(echo "${_comment}" | jq --raw-output ."Author")" test

	# test update comment
	update_comment "${_id}" "${_cid}"

	_comment="$(get_comment "${_id}" "${_cid}")"

	ensure assert_eq "$(echo "${_comment}" | jq --raw-output ."Comment")"
	ensure assert_eq "$(echo "${_comment}" | jq --raw-output ."Author")" test2

	# test delete comment
	delete_comment "${_id}" "${_cid}"

	# test empty comments
	ensure assert_eq "$(get_all_comments ${_id})" {}

	# test delete review
	delete_review "${_id}"
	ensure assert_eq "$(get_all_reviews)" {}

	say Passed!
}

get_all_reviews()
{
	curl -s ${HOST}:3000/api/v1/reviews
}

create_review()
{
	local _tmp
	_tmp=$(curl -s -X POST --data '{"Title": "test", "Author": "test", "Article": "test"}' ${HOST}:3000/api/v1/reviews)
	_tmp=${_tmp%\"}
	_id=${_tmp#\"}
}

get_review()
{
	curl -s ${HOST}:3000/api/v1/reviews/$1
}

update_review()
{
	curl -s -X PUT --data '{"Title": "test2"}' ${HOST}:3000/api/v1/reviews/$1
}

delete_review()
{
	curl -s -X DELETE ${HOST}:3000/api/v1/reviews/$1
}

get_all_comments()
{
	curl -s ${HOST}:3000/api/v1/reviews/$1/comments
}

create_comment()
{
	local _tmp
	_tmp=$(curl -s -X POST --data '{"Author": "test", "Comment": "test"}' ${HOST}:3000/api/v1/reviews/$1/comments)
	_tmp=${_tmp%\"}
	_cid=${_tmp#\"}
}

get_comment()
{
	curl -s ${HOST}:3000/api/v1/reviews/$1/comments/$2
}

update_comment()
{
	curl -s -X PUT --data '{"Author": "test2"}' ${HOST}:3000/api/v1/reviews/$1/comments/$2
}

delete_comment()
{
	curl -s -X DELETE ${HOST}:3000/api/v1/reviews/$1/comments/$2
}

assert_eq()
{
	[ "$1" = "$2" ] || return 1
}

ensure()
{
	if ! "$@"; then err "command failed: $*"; fi
}

check_empty()
{
	[ -n "$1" ] || err "cannot be empty"
}

# utility function to indicate to user that program is needed
need_cmd()
{
	if ! check_cmd "$1"; then
		err "need $1 (command not found)"
	fi
}

# utility function to check if a program exectuable is callable
check_cmd()
{
	type "$1" >/dev/null 2>&1
}

# utility function to echo error and exit program
err()
{
	say "$@" >&2
	exit 1
}

# utility function to print with a tagline
say()
{
	echo "e2e: $@"
}

main "$@" || exit 1
